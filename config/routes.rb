Rails.application.routes.draw do
  devise_for :users
  # :controllers => {:registrations => "registrations"}
  devise_scope :user do

  	get 'login' => 'devise/session#new', as: :login
  	get 'signup', to: 'devise/registration#new'
  end
 root to: 'pages#index'
end
